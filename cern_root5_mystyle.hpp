#pragma once

#pragma warning(disable:4996)

#include <TStyle.h>
#include <TROOT.h>
#include <TColor.h>

#include <sstream>
#include <iostream>

TStyle* MyStyle(bool ShowStats, bool Color, int NColor);

void SetMyStyle (bool ShowStats, bool Color, int NColor = 255, bool Bold = false)
{
	std::cout << "\nApplying MyStyle settings...\n" << std::endl ;
	TStyle* Style = MyStyle(ShowStats, Color, NColor, Bold);
	
	gROOT->SetStyle("Plain");
	gROOT->SetStyle("MyStyle");
	gROOT->ForceStyle();
}

TStyle* MyStyle(bool ShowStats, bool Color, int NColor, bool Bold)
{
	TStyle *MyStyle = new TStyle("MyStyle", "Based on Atlas style");

	//http://kkuatx.hep.kyokyo-u.ac.jp/~ryuichi/doxy-tbmon2/html/_atlas_style_8cc_source.html

	// use plain black on white colors
	int icol = 0; // WHITE
	short scol = 0;
	MyStyle->SetFrameBorderMode(icol);
	MyStyle->SetFrameFillColor(scol);
	MyStyle->SetCanvasBorderMode(icol);
	MyStyle->SetCanvasColor(scol);
	MyStyle->SetPadBorderMode(icol);
	MyStyle->SetPadColor(scol);
	MyStyle->SetStatColor(scol);
	//MyStyle->SetFillColor(icol); // don't use: white fill color floa *all* objects

	MyStyle->SetStatBorderSize(1);
	MyStyle->SetTitleBorderSize(1);

	// set the paper
	//MyStyle->SetPaperSize(20,26);

	// set the margin sizes 0.12 to 0.15
	float margin = 0.12;
	MyStyle->SetPadTopMargin(margin);
	MyStyle->SetPadRightMargin(margin + 0.03f);
	MyStyle->SetPadBottomMargin(margin);
	MyStyle->SetPadLeftMargin(margin);

	// set title offsets (for axis label)
	MyStyle->SetTitleXOffset(1.3f);
	MyStyle->SetTitleYOffset(1.3f);

	// use large fonts
	short font = 42; // Helvetica
	float tsize = 0.04f;
	MyStyle->SetTextFont(font);

	MyStyle->SetTextSize(tsize);
	MyStyle->SetLabelFont(font, "x");
	MyStyle->SetTitleFont(font, "x");
	MyStyle->SetLabelFont(font, "y");
	MyStyle->SetTitleFont(font, "y");
	MyStyle->SetLabelFont(font, "z");
	MyStyle->SetTitleFont(font, "z");

	MyStyle->SetLabelSize(tsize, "x");
	MyStyle->SetTitleSize(tsize, "x");
	MyStyle->SetLabelSize(tsize, "y");
	MyStyle->SetTitleSize(tsize, "y");
	MyStyle->SetLabelSize(tsize, "z");
	MyStyle->SetTitleSize(tsize, "z");

	MyStyle->SetLabelOffset(tsize / 4, "x");
	MyStyle->SetLabelOffset(tsize / 4, "y");
	MyStyle->SetTitleOffset(1.3f, "z");

	// use bold lines and markers
	MyStyle->SetMarkerStyle(1);
	MyStyle->SetMarkerSize(1.0f);
	//MyStyle->SetHistLineWidth(2);
	//MyStyle->SetLineStyleString(2, "[12 12]"); // postscript dashes
	MyStyle->SetHistLineWidth(Bold ? 2: 1);
	MyStyle->SetLineStyleString(1, "[]"); // postscript solid

	// Fit line SetHistLineWidth
	MyStyle->SetLineWidth(Bold ? 2: 1);
	MyStyle->SetFuncWidth(Bold ? 2: 1);
	
	//Legend add on 20170210
	MyStyle->SetLegendBorderSize(1);
	MyStyle->SetLegendFillColor(0);
	MyStyle->SetLegendFont(font);
	//MyStyle->SetLegendTextSize(tsize);

	// get rid of X error bars and y error bar caps
	//MyStyle->SetErrorX(0.001);

	// do not display any of the standard histogram decorations
	MyStyle->SetTitleFillColor(0);
	MyStyle->SetOptTitle(1); //title =0, not display

	MyStyle->SetStatH(0.1); //Stats size
	MyStyle->SetStatW(0.25);//Stats size

	if(ShowStats){
		MyStyle->SetOptStat("emruo");
		MyStyle->SetOptFit(1111);
	}
	else{
		MyStyle->SetOptStat(kFALSE);
		MyStyle->SetOptFit(kFALSE);
	}

	// put tick marks on top and RHS of plots
	MyStyle->SetPadTickX(1);
	MyStyle->SetPadTickY(1);

	MyStyle->SetPalette(1);

	const int NRGBs = 5;
	const int NCont = 255;

	//2次元ヒストグラムのグラデーションを変える(お好みで)
	if(Color){
		Double_t  stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
		Double_t  red  [NRGBs] = { 0.00, 0.00, 0.87, 1.00, 0.51 };
		Double_t  green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
		Double_t  blue [NRGBs] = { 0.51, 1.00, 0.12, 0.00, 0.00 };
		TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NColor);
	}
	else{
		Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
		Double_t red  [NRGBs] = { 1.00, 0.84, 0.61, 0.34, 0.00 };
		Double_t green[NRGBs] = { 1.00, 0.84, 0.61, 0.34, 0.00 };
		Double_t blue [NRGBs] = { 1.00, 0.84, 0.61, 0.34, 0.00 };
		TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NColor);
	}

	MyStyle->SetNumberContours(NColor);

	//ROOTの標準の色を変える(お好みで)
	//0 白
	//0 黒
	//アクセントカラー
	gROOT->GetColor(2)->SetRGB(255.0f / 255.0f, 40.0f / 255.0f, 0.0f / 255.0f); // 赤色
	gROOT->GetColor(3)->SetRGB(250.0f / 255.0f, 245.0f / 255.0f, 0.0f / 255.0f); // 黄色
	gROOT->GetColor(4)->SetRGB(53.0f / 255.0f, 161.0f / 255.0f, 107.0f / 255.0f); // 緑色
	gROOT->GetColor(5)->SetRGB(0.0f / 255.0f, 65.0f / 255.0f, 255.0f / 255.0f); // 青色
	gROOT->GetColor(6)->SetRGB(102.0f / 255.0f, 204.0f / 255.0f, 255.0f / 255.0f); // 空色
	gROOT->GetColor(7)->SetRGB(255.0f / 255.0f, 153.0f / 255.0f, 160.0f / 255.0f); // ピンク
	gROOT->GetColor(8)->SetRGB(255.0f / 255.0f, 153.0f / 255.0f, 0.0f / 255.0f); // オレンジ
	gROOT->GetColor(9)->SetRGB(154.0f / 255.0f, 0.0f / 255.0f, 121.0f / 255.0f); // 紫
	//gROOT->GetColor(10)->SetRGB(102.0f / 255.0f, 51.0f / 255.0f, 0.0f / 255.0f); // 茶色

	//ベースカラー
	gROOT->GetColor(11)->SetRGB(255.0f / 255.0f, 209.0f / 255.0f, 209.0f / 255.0f); // 明るいピンク
	gROOT->GetColor(12)->SetRGB(255.0f / 255.0f, 255.0f / 255.0f, 153.0f / 255.0f); // クリーム
	gROOT->GetColor(13)->SetRGB(203.0f / 255.0f, 242.0f / 255.0f, 102.0f / 255.0f); // 明るい黄緑
	gROOT->GetColor(14)->SetRGB(180.0f / 255.0f, 235.0f / 255.0f, 250.0f / 255.0f); // 明るい空色
	gROOT->GetColor(15)->SetRGB(237.0f / 255.0f, 197.0f / 255.0f, 143.0f / 255.0f); // ベージュ
	gROOT->GetColor(16)->SetRGB(135.0f / 255.0f, 231.0f / 255.0f, 176.0f / 255.0f); // 明るい緑
	gROOT->GetColor(17)->SetRGB(199.0f / 255.0f, 178.0f / 255.0f, 222.0f / 255.0f); // 明るい紫

	//無彩色
	gROOT->GetColor(18)->SetRGB(200.0f / 255.0f, 200.0f / 255.0f, 203.0f / 255.0f); // 明るいグレー
	gROOT->GetColor(19)->SetRGB(127.0f / 255.0f, 135.0f / 255.0f, 143.0f / 255.0f); // グレー
	
	//以下のサイトを参照
	//http://jfly.iam.u-tokyo.ac.jp/colorset/CUD_color_set_GuideBook_2013.pdf

	return MyStyle;

}
